﻿// 1512424Lab03.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512424Lab03.h"
#include <string>
#include <iostream>
#include <fstream>
#include <windowsx.h>
using namespace std;


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512424LAB03, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512424LAB03));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512424LAB03));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512424LAB03);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
HWND edit;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		RECT rect;
		GetClientRect(hWnd, &rect);// lay kich thuoc khung ben trong cua so
		edit = CreateWindow(L"edit", L"", WS_HSCROLL | WS_VSCROLL | WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | ES_MULTILINE | ES_AUTOVSCROLL, 0, 0, rect.right, rect.bottom, hWnd, 0, 0, 0, 0);
		break;
	}
	case WM_SETFOCUS:
	{
		SetFocus(edit);
		break;
	}
	case WM_SIZE:
	{
		MoveWindow(edit, 0, 0, LOWORD(lParam), HIWORD(lParam), TRUE);
		break;
	}
    case WM_COMMAND:	
        {
			wfstream fo;
			ifstream fi;
			string s;
			string buffer1;
			WCHAR* buffer = NULL;
			WCHAR* temp = NULL;
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_FILE_OPEN:
				fi.open("1512424.txt");

				while (!fi.eof())
				{
					getline(fi, s);
					buffer1 += s;
					buffer1 += "\r\n";

				}
				temp = new WCHAR[buffer1.size() + 1];

				for (int i = 0; i < buffer1.size(); i++)
				{
					temp[i] = buffer1[i];
				}
				temp[buffer1.size()] = NULL;
				SetWindowText(edit, temp);
				fi.close();

				break;
			case ID_FILE_SAVE:
				int size;
				size = GetWindowTextLength(edit);
				buffer = new WCHAR[size + 1];
				GetWindowText(edit, buffer, size + 1);
				fo.open("1512424_.txt", ios::out);
				fo << buffer;
				fo.close();
				delete[]buffer;
				MessageBox(edit, L"Đã lưu file!", L"SAVE", MB_OK | MB_ICONINFORMATION);
				break;

            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
			case ID_EDIT_COPY:
				SendMessage(edit, WM_COPY, 0, 0);
				break;
			case ID_EDIT_CUT:
				SendMessage(edit, WM_CUT, 0, 0);
				MessageBox(edit, L"Đã cut !", L"CUT", MB_OK);
				break;
			case ID_EDIT_PASTE:
				SendMessage(edit, WM_PASTE, 0, 0);
				break;
            case IDM_FILE_EXIT:
			{
				int result = MessageBox(0, L"Ban co muon luu file ?", L"SAVE", MB_OKCANCEL);
				if (1 == result)
				{
					int size;
					size = GetWindowTextLength(edit);
					buffer = new WCHAR[size + 1];
					GetWindowText(edit, buffer, size + 1);
					fo.open("1512424_.txt", ios::out);
					fo << buffer;
					fo.close();
					delete[]buffer;
					MessageBox(edit, L"Đã lưu file!", L"SAVE", MB_OK | MB_ICONINFORMATION);
					DestroyWindow(hWnd);
				}
				else
					DestroyWindow(hWnd);
				break;
			}
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}