Họ tên:Nguyễn Vạn Phước
MSSV:1512424
Chức năng thực hiện được: 
- Sự kiện chính:
1. Đã tạo ra giao diện có một edit control để nhập liệu.

2. Lưu lại dưới dạng file text. Code cứng tên tập tin.

3. Mở một tập tin, đọc nội dung và hiển thị để chỉnh sửa tiếp. Code cứng tên tập tin

4. Hỗ trợ 3 thao tác Cut - Copy - Paste.
- 3 luồng sự kiện phụ:
1. Sau khi lưu file thì hiện "đã lưu !"
2. Sau khi thực hiện thao tác Cut thì hiện đã cut thành công thông báo cho người dùng biết
3. Khi bấm exit để thoát , chương trình sẽ hỏi người dùng có muồn lưu file không ? 
Link bitbucket:https://bitbucket.org/phuocnguyen105/windev